var $=jQuery;

var goContainer;
var utility={
	populateTimeDiff:function(datetime)
	{	
		var diff = new Date().getTime() - new Date(datetime).getTime();
		var days = Math.floor(diff / (1000 * 60 * 60 * 24));
		diff -= days * (1000 * 60 * 60 * 24);
		var hours = Math.floor(diff / (1000 * 60 * 60));
		diff -= hours * (1000 * 60 * 60);
		var mins = Math.floor(diff / (1000 * 60));
		diff -= mins * (1000 * 60);
		var timeString = "";
		if (days > 0)
		{
			timeString = timeString + days + " days, ";
		}
		if (hours > 0) {
			timeString = timeString + hours + " hours, ";
		}
		if (mins > 0)
		{
			timeString = timeString + mins + " minutes ago";
		}
	
		if (timeString === "") timeString = "a few moments ago";
		return timeString;
	},
	nameMap:{intensity:{High:"red",Medium:"yellow",Low:"green"},type:{Traffic:"traffic"}},
	checkIntensityType:function(incidentIntensity, type)
	{
	    var imageSrc;
	    if(type=="Traffic")
	    {
	            if(incidentIntensity=="High")
	            {
	                imageSrc="Image/Intensity/rsz_1incident-traffic-red.png";
	            }
	            else if(incidentIntensity=="Medium")
	            {
	                imageSrc="Image/Intensity/rsz_incident-traffic-yellow.png";
	            }
	            else if(incidentIntensity=="Low")
	            {
	                imageSrc="Image/Intensity/rsz_incident-traffic-green.png";
	            }
	    }
	    else if(type=="Crowd")
	    {
	            if(incidentIntensity=="High")
	            {
	                imageSrc="Image/Intensity/rsz_incident-crowd-traffic-red.png";
	            }
	            else if(incidentIntensity=="Medium")
	            {
	                imageSrc="Image/Intensity/rsz_incident-crowd-yellow.png";
	            }
	            else if(incidentIntensity=="Low")
	            {
	                imageSrc="Image/Intensity/rsz_incident-crowd-green.png";
	            }
	    }
	    else if(type=="Danger")
	    {
	            if(incidentIntensity=="High")
	            {
	                imageSrc="Image/Intensity/rsz_incident-danger-red.png";
	            }
	            else if(incidentIntensity=="Medium")
	            {
	                imageSrc="Image/Intensity/rsz_incident-danger-yellow.png";
	            }
	            else if(incidentIntensity=="Low")
	            {
	                imageSrc="Image/Intensity/rsz_incident-danger-green.png";
	            }
	    }
	    else if(type=="Road Accident")
	    {
	            if(incidentIntensity=="High")
	            {
	                imageSrc="Image/Intensity/rsz_incident-accident-red.png";
	            }
	            else if(incidentIntensity=="Medium")
	            {
	                imageSrc="Image/Intensity/rsz_incident-accident-yellow.png";
	            }
	            else if(incidentIntensity=="Low")
	            {
	                imageSrc="Image/Intensity/rsz_incident-accident-green.png";
	            }
	    }
	    else if(type=="Vip")
	    {
	            if(incidentIntensity=="High")
	            {
	                imageSrc="Image/Intensity/rsz_1incident-vip-red.png";
	            }
	            else if(incidentIntensity=="Medium")
	            {
	                imageSrc="Image/Intensity/rsz_incident-vip-yellow.png";
	            }
	            else if(incidentIntensity=="Low")
	            {
	                imageSrc="Image/Intensity/rsz_incident-vip-green.png";
	            }
	    }
	    else if(type=="Construction")
	    {
	            if(incidentIntensity=="High")
	            {
	                imageSrc="Image/Intensity/rsz_incident-construction-red.png";
	            }   
	            else if(incidentIntensity=="Medium")
	            {
	                imageSrc="Image/Intensity/rsz_incident-construction-yellow.png";
	            }
	            else if(incidentIntensity=="Low")
	            {
	                imageSrc="Image/Intensity/rsz_incident-construction-green.png";
	            }
	    }
	      
	
	    return "http://gobd.co/app/"+imageSrc;				
	},
	checkClusterType:function(clusterintensity)
	{
	    var clusterImageSrc;
	    if(clusterintensity=="High")
	    {
	        clusterImageSrc="Image/rsz_cluster_red.jpg";
	         console.log(clusterImageSrc);
	    }   
	    else if(clusterintensity=="Medium")
	    {
	        clusterImageSrc="Image/Intensity/rsz_cluster-yellow.png";
	    }
	    else if(clusterintensity=="Low")
	    {
	        clusterImageSrc="Image/Intensity/rsz_cluster-green.png";
	    }
	    

	   
	    return "http://gobd.co/app/"+clusterImageSrc;
	},
	checkManualIntersectionType:function(manualIntensity)
	{
	    var manualimageSrc="";
	      
	        if(manualIntensity=="High")
	        {
	             manualimageSrc="Image/Intersection/rsz_intersection-red.png";
	        }   
	        else if(manualIntensity=="Medium")
	        {
	             manualimageSrc="Image/Intersection/rsz_intersection-yellow.png";
	        }
	        else if(manualIntensity=="Low")
	        {
	             manualimageSrc="Image/Intersection/rsz_intersection-green.png";
	        }

	        return "http://gobd.co/app/"+manualimageSrc;
	       
	}		
}


var goTrafficFeature={
	Path:[],
	incidentColor:{"High":"#e53935","Medium":"#FFB300","Low":"#4CAF50"},
	map:null,
	vtsUpdate:true,
	rect:null,
	zoom:null,
	data:{
		path:{
			manual:{
				data:null,
				draw:function(){
					goTrafficFeature.clear(this.path);
					for(var i=0;i<this.data.length;i++)
					{
						goTrafficFeature.drawPaint(this.data[i]._id.Path.coordinates,this.data[i].AvgIntensity,this)
					}					
				},
				path:[]
			},
			vts:{
				data:null,
				draw:function(){
					goTrafficFeature.clear(this.path);
					
					for(var i=0;i<this.data.length;i++)
					{
						
						goTrafficFeature.drawPaint(this.data[i]._id.Path.coordinates,this.data[i].AvgIntensity,this)
					}						
				},
				path:[]
			}
		},
		marker:{
			vts:{
				data:null,
				draw:function(){
					console.log(this.data);
					goTrafficFeature.clear(this.marker);
					
					for(var i=0;i<this.data.length;i++)
					{
						if(goTrafficFeature.zoom > 12 )
						{
						goTrafficFeature.drawIntensityMarker(this.data[i].Point.coordinates[0] + "," + this.data[i].Point.coordinates[1],this.data[i].IncidentIntensity, this.data[i].Type, this.data[i].Message, this.data[i].DateTime,this)
						}
						else
						{
						goTrafficFeature.drawClusterMarker(this.data[i].Point.coordinates[0] + "," + this.data[i].Point.coordinates[1],this.data[i].ClusterIntensity.IntensityLevel,this);	
						}
					}						
				},
				marker:[]
			},
			manual:{
				data:null,
				draw:function(){
					goTrafficFeature.clear(this.marker);
					for(var i=0;i<this.data.length;i++)
					{
						for(var j=0;j<this.data[i].Intersections.length;j++)
						{
							var item=this.data[i].Intersections[j];
							try{
							//drawManualIntersectionDataMarker:function(latlng, manualIntensity,times,store)
							goTrafficFeature.drawManualIntersectionDataMarker(item.Location.coordinates[0]+","+item.Location.coordinates[1],item.Intensity.Intensity,item.Intensity.TimeDef+"ago",this)
							}catch(e){
								
							}
						}
					}
				},
				marker:[]
			}
			
			
		}
	},
	init:function(map){
		this.map=map;
		this.updateVTS();
		this.loadData();
	},
	loadData:function(){
		$.getJSON("http://gobdhodor.cloudapp.net:1337/getWayStatus", function(data)
		{			
			goTrafficFeature.data.path.manual.data=data;
			goTrafficFeature.data.path.manual.draw();
			//goTrafficFeature.drawPaint();
		});		
		$.getJSON("http://gobdhodor.cloudapp.net:1337/getStatus", function(data)
		{
			goTrafficFeature.data.marker.manual.data=data;
			goTrafficFeature.data.marker.manual.draw();	
			goTrafficFeature.drawRightList();			
		});		
	},
	clear:function(store){
		for(var i=0;i<store.length;i++)
		{
			store[i].setMap(null);
		}
		store=[];
	}
	,
	drawPaint:function(coord,intensity,store){
			var poly=[];
			for(var j=0;j<coord.length;j++)
			{
				poly.push(new google.maps.LatLng(coord[j][1], coord[j][0]));
			}
			var mapPath=new google.maps.Polyline(
			{
			    path: poly,
			    strokeColor: this.incidentColor[intensity],
			    strokeOpacity: 1.0,
			    strokeWeight: 5,
			 });
			mapPath.setMap(this.map);
			store.path.push(mapPath);				
	},
	drawIntensityMarker:function(latlng, incidentIntensity, type, msg, datetime,store){
		
		try
		{
			latlng = latlng.split(",");
			times=utility.populateTimeDiff(datetime);
			var intensityImagePath=utility.checkIntensityType(incidentIntensity, type)
			console.log(intensityImagePath);
			var mess = msg;
			var zoom = goTrafficFeature.map.getZoom();
			var message="";
			if (((mess == null)) || ((mess === "")))
			{
				message = type;
				message = message + "\n\n" + times;
			} 
			else
			{
				message = mess;
				message = message + "\n\n" + times;
			}
			var pinImage = new google.maps.MarkerImage(intensityImagePath,
				new google.maps.Size(100, 100),
				new google.maps.Point(0, 0),
				new google.maps.Point(10, 34));
			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(parseFloat(latlng[1]), parseFloat(latlng[0])),
				map: goTrafficFeature.map,
				icon: pinImage,
				title: message,
			});
			marker.setMap(goTrafficFeature.map);
			store.marker.push(marker);
		}
		catch (e)
		{
			console.log("Failed To Pin Intensity Data Markers In the Map",e);
		}		
	},
	drawClusterMarker:function(latlng, clusterintensity,store){
		try
		{
			latlng = latlng.split(",");
			var clusterImagePath=utility.checkClusterType(clusterintensity);
			var pinImage = new google.maps.MarkerImage(clusterImagePath,
				new google.maps.Size(100, 100),
				new google.maps.Point(0, 0),
				new google.maps.Point(10, 34));
			var marker = new google.maps.Marker(
			{
				position: new google.maps.LatLng(parseFloat(latlng[1]), parseFloat(latlng[0])),
				map: goTrafficFeature.map,
				icon: pinImage,
				title: "",
			});
			store.marker.push(marker);
		}
		catch (e)
		{
			console.log("Failed To Pin clustermarkers Data Markers In the Map",e);
		}		
	},
	drawManualIntersectionDataMarker:function(latlng, manualIntensity,times,store)
	{
		try
		{
			latlng = latlng.split(",");
			var manualImagePath=utility.checkManualIntersectionType(manualIntensity);
			
			var pinImage = new google.maps.MarkerImage(manualImagePath,
				new google.maps.Size(100, 100),
				new google.maps.Point(0, 0),
				new google.maps.Point(10, 34));
			var marker = new google.maps.Marker(
			{
				position: new google.maps.LatLng(parseFloat(latlng[1]), parseFloat(latlng[0])),
				map: goTrafficFeature.map,
				icon: pinImage,
				title:times
			});
			store.marker.push(marker);
			
		}
		catch (e)
		{
			console.log("Failed To Pin clustermarkers Data Markers In the Map",e);
		}
	},		
	mergeData:function(){
		
	},
	updateVTS:function()
	{
		google.maps.event.addListener(map, 'center_changed', function()
		{
			goTrafficFeature.mapDataChanged();
		});
		
		google.maps.event.addListener(map, 'bounds_changed', function()
		{
			goTrafficFeature.mapDataChanged();
		});		
		
		setInterval(function()
		{
			if(goTrafficFeature.update&&goTrafficFeature.rect!=null)
			{
			
				$.getJSON("http://gobdthor.cloudapp.net/Autotraffic/findWaysByBounds?lat1=" + goTrafficFeature.rect.Ca.j + "&lon1=" + goTrafficFeature.rect.va.j + "&lat2=" + goTrafficFeature.rect.Ca.j + "&lon2=" + goTrafficFeature.rect.va.k + "&lat4=" + goTrafficFeature.rect.Ca.k + "&lon4=" + goTrafficFeature.rect.va.j + "&lat3=" + goTrafficFeature.rect.Ca.k + "&lon3=" + goTrafficFeature.rect.va.k + "&zoom=" + goTrafficFeature.zoom, function(data) {			
				
					goTrafficFeature.data.path.vts.data=data;
					goTrafficFeature.data.path.vts.draw();
					
				});	
				$.getJSON("http://gobdthor.cloudapp.net/incident/findIncidentByBounds?lat1=" + goTrafficFeature.rect.Ca.j + "&lon1=" + goTrafficFeature.rect.va.j + "&lat2=" + goTrafficFeature.rect.Ca.j + "&lon2=" + goTrafficFeature.rect.va.k + "&lat4=" + goTrafficFeature.rect.Ca.k + "&lon4=" + goTrafficFeature.rect.va.j + "&lat3=" + goTrafficFeature.rect.Ca.k + "&lon3=" + goTrafficFeature.rect.va.k + "&zoom=" + goTrafficFeature.zoom, function(data) {			
				
					goTrafficFeature.data.marker.vts.data=data;
					goTrafficFeature.data.marker.vts.draw();
					
				});					
				goTrafficFeature.update = false;			
			}
			
		},100);
	},
	mapDataChanged:function(){
		var rb = goTrafficFeature.convertBoundObject(goTrafficFeature.map.getBounds());
		if (goTrafficFeature.zoom == goTrafficFeature.map.getZoom() && rb.va.j > this.rect.va.j && rb.va.k < this.rect.va.k && rb.Ca.j > this.rect.Ca.j && rb.Ca.k < this.rect.Ca.k)
		{
			
			return false;
		}
		this.zoom = this.map.getZoom();
		this.rect = this.extendedBound(goTrafficFeature.map.getBounds());
		this.update = true;		
	},
	convertBoundObject:function(bound){
		var se ={
					j: bound.getSouthWest().lat(),
					k: bound.getSouthWest().lng()
				}
				
		var ne ={
					j: bound.getNorthEast().lat(),
					k: bound.getNorthEast().lng()
				}
			
		return {
					Ca: {
							j: se.j,
							k: ne.j
						},
					va: {
							j: se.k,
							k: ne.k
						}
				};		
	},
	extendedBound:function(bound){
		var rect = goTrafficFeature.convertBoundObject(bound);
		var vaRange = Math.abs(rect.va.j - rect.va.k);
		var caRange = Math.abs(rect.Ca.j - rect.Ca.k);
		rect.va.j -= vaRange;
		rect.va.k += vaRange;
		rect.Ca.j -= caRange;
		rect.Ca.k += caRange;
		return rect;		
	},
	drawRightList:function(){
		//goTrafficFeature.addGoogleFont("Roboto:300,400,500,700");
		var left=parseInt($(goContainer).css("margin-left").replace("px",""));
		//alert(left);
		var top=parseInt($(goContainer).css("margin-top").replace("px",""));
		//alert(top);
		var width=$(goContainer).width();
		//alert(width);
		var height=$(goContainer).height();
		$(goContainer).before("<div style='font-family:\"Roboto\";font-size:15px;height:25px;width:400px;z-index:1;position:absolute;margin-top:"+(height-40)+"px;margin-left:"+(left+5)+"px'><img style='margin-top:5px;color:darkred;width:12px;height:12px;' alt='gobd' src='http://gobd.co/app/Image/copyright.png'><strong>GObd,</strong> 2015 || <strong>All Rights Reserved.</strong><a href='https://play.google.com/store/apps/details?id=co.gobd.loki.loki' target='_black'><img src='http://gobd.co/app/Image/playstoreicon.png' alt='Get it on Google Play' style='vertical-align:top;margin-top:-12px;'></a></div>");
		
		if(width<600)
		{
			return 0;
		}
		$(goContainer).before("<div id='gotraffic_right' style='font-family: \"Roboto-Light\";position:absolute;background-color:white;width:330px;height:"+(height-100)+"px;margin-top:"+(50)+"px;margin-left:"+(left+width-350)+"px;z-index:1;box-shadow:2px 2px 2px 2px #888888;border-radius:8px;'>"+
							"<div style='font-size:0px;'><span style='width:80px;display:inline-block;margin-top:15px;font-size:15px;height:25px;text-align:center;line-height:25px;border:none;margin-left:25px;color:white;background-color:#43A047;vertical-align:top;'>Smooth</span>"+
							"<span style='width:120px;display:inline-block;margin-top:15px;font-size:15px;height:25px;text-align:center;line-height:25px;border:none;color:white;background-color:#ffc107;vertical-align:top;'>Slow but moving</span>"+
							"<span style='width:80px;display:inline-block;margin-top:15px;font-size:15px;height:25px;text-align:center;line-height:25px;border:none;color:white;background-color:#e53935;vertical-align:top;'>Jammed</span>"+
							
							"</div>");
		$("#gotraffic_right").append("<div id='intensity_report' style='font-family:\"Roboto\";width:330px;height:"+(height-140)+"px;overflow:hidden;'><div id='report_list' style='margin-top:-10px;padding:0px;margin-left:-8px;margin-top:5px;width:385px;height:"+(height-150)+"px;overflow-y:scroll;'><div></div>");
							



	report=goTrafficFeature.data.marker.manual.data;
	var list = {};
	var rep = {};

	function sortByDate(a, b) 
	{
		var val = new Date(a.LastUpdated) - new Date(b.LastUpdated);
		if (!a.AvgIntensity)
		{
			return 1;
		}
		else if (!b.AvgIntensity)
		{
			return -1;
		}
		else
		{
			return val * -1;
		}
	}
	report.sort(sortByDate);

	for (var i = 0; i < report.length; i++)
	{
		list[report[i]._id] =
		{
			name: report[i].Name,

			sorttime: report[i].LastUpdated === undefined ? "" : report[i].LastUpdated,

			time: report[i].TimeDef === undefined ? "" : report[i].TimeDef + " ago ",
			child: [],
			avgIntensity: report[i].AvgIntensity === undefined ? "" : report[i].AvgIntensity
		};
		for (var j = 0; j < report[i].Intersections.length; j++)
		{
			list[report[i]._id].child.push([report[i].Intersections[j].Name, report[i].Intersections[j]._id, report[i].Intersections[j].Location.coordinates]);
			rep[report[i].Intersections[j]._id] =
			{
				intensity: report[i].Intersections[j].Intensity === undefined ? undefined : report[i].Intersections[j].Intensity.Intensity,
				time: report[i].Intersections[j].Intensity === undefined ? undefined : report[i].Intersections[j].Intensity.TimeDef,
			};
		}
	}
	$("#report_list").html("");

	var ul = $("<ul class='tree'><ul id='innerTree'></ul></ul>");
	var it = $("#innerTree", ul);
	var menu_color;

	for (var item in list)
	{
		menu_color=goTrafficFeature.incidentColor[list[item].avgIntensity];

		var menu = $("<li class='menu'><div type='radio' style='position:absolute;top:7px;left:-8px;width:16px;height:16px;background-color:" + menu_color + ";border-radius:50%;z-index:999;'></div><span>" + list[item].name + "</span>&nbsp;<span style='postion:inline;z-index:1;font-size:12px;color:black;margin-left:65px>" + list[item].time + "</span></li><div class='time' style='postion:inline;z-index:1;margin-left:55px'>" + list[item].time + "</div>");
		var sm = $("<ul style='display:none;'></ul>");
		var totalVal = 0;

		for (var i = 0; i < list[item].child.length; i++)
		{
			var times = "";
			if (typeof rep[list[item].child[i][1]].time != "undefined")
			{
				times =  rep[list[item].child[i][1]].time + " ago";
			}
		
            if(typeof rep[list[item].child[i][1]].intensity !="undefined")
            {
                intensityValue = rep[list[item].child[i][1]].intensity;
                //addManualIntersectionDataMarker(list[item].child[i][2][0] + "," + list[item].child[i][2][1],intensityValue,times);
            }
            else
            {
            	intensityValue=null;
            	sub_color = "gray";
            }
            var intensValue = rep[list[item].child[i][1]].intensity;
            sub_color=goTrafficFeature.incidentColor[intensValue];
			sm.append("<li class='sub_menu' onclick='' latlng='" + list[item].child[i][2][0] + "," + list[item].child[i][2][1] + "'><div type='radio' style='position:absolute;top:5px;left:-8px;width:16px;height:16px;background-color:" + sub_color + ";border-radius:50%;z-index:999;'></div>" + list[item].child[i][0] +"<span class='time'> "+ times + "</span></li>");
		}
		$("div", menu).css("background-color:",  + menu_color );
		menu.append(sm);
		it.append(menu);
	}

	$("#report_list").append(ul);

	$("#report_list .menu span").click(function() 
	{
		var sub = $("ul", $(this).parent());
		if (sub.css("display") == "none")
		{
			sub.slideDown(300);
		}
		else
		{
			sub.slideUp(300);
		}
	});
	
	$("#report_list .sub_menu").click(function() 
	{
		var latln=$(this).attr("latlng").split(",");
		goTrafficFeature.map.setCenter(new google.maps.LatLng(parseFloat(latln[1]), parseFloat(latln[0])), 12);
	});	
	
	},
	addGoogleFont:function(FontName) {
    	$("head").append("<link href='https://fonts.googleapis.com/css?family=" + FontName + "' rel='stylesheet' type='text/css'>");
	}	
}
function goInitialize() {
	var center={
			lat: 23.7463601,
			lng: 90.3946522
		}
		
	try{	
		var location=window.top.location.href;
		
		var params=location.split("?")[1].split("&");
	
		for(var i=0;i<params.length;i++)
		{
			var param=params[i].split("=");
			
			if(param[0].localeCompare("latlng")==0)
			{
				var latlon=param[1].split(",");
				console.log(latlon);
				center={
					lat:parseFloat(latlon[1]),
					lng:parseFloat(latlon[0])
				}
			}
		}
	}catch(e){}
    var mapOptions = {
		center:center,
		panControl: false,
		zoomControl: true,
		mapTypeControl: false,
		streetViewControl: false,
		zoomControlOptions:
		{
			style: google.maps.ZoomControlStyle.LARGE,
			position: google.maps.ControlPosition.LEFT_CENTER
		},
		scaleControl: true,
		zoom: 17,
		disableDefaultUI: false
    };
    map = new google.maps.Map($(goContainer)[0],
            mapOptions);
            
    goTrafficFeature.init(map);        
  }
  

$.fn.goTraffic=function()
{
	goContainer=this;
    function loadScript(src,callback){
    var script = document.createElement("script");
    script.type = "text/javascript";
    if(callback)script.onload=callback;
    document.getElementsByTagName("head")[0].appendChild(script);
    script.src = src;
  	}
  loadScript('https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=AIzaSyA-EwfDu1Ik_xGwk65JHdmdWpxc1Zpp9g0&sensor=false&callback=goInitialize',
              function(){console.log('google-loader has been loaded, but not the maps-API ');});
}



